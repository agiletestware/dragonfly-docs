Overview
========

Dragonfly is a plugin for Smartbear's soapUI and ReadyAPI product. Dragonfly plugin provides integration between soapUI and ReadyAPI with HP ALM.

Users can export tests and testrun results from soapUI and ReadyAPI into HP ALM automatically by simple configurations and without writing any code.

Dragonfly is compatible with all versions of soapUI, ReadyAPI, and HP ALM/Quality Center.

![](img/what_is_dragonfly/hp-alm.png)

![](img/what_is_dragonfly/SmartBear-Ready-API-Logo.jpg)

![](img/what_is_dragonfly/SoapUI-NG-Pro-Color-Horizontal-version_Logo.png)

![](img/what_is_dragonfly/soapui-logo.png)
