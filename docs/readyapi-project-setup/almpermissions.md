Dragonfly plugin creates tests and uploads test results in HP ALM's TestPlan and TestLab. As such, the user needs to have appropriate permission in HP ALM.

## TestPlan Permissions

- ==Create== TestPlan folder
- ==Create== Tests in TestPlan
- ==Create, Update, Delete== Design Steps in the testcase

[![tesplan permissions](../img/soapui_project_setup/testplan-permissions.png)](../img/soapui_project_setup/testplan-permissions.png)

## TestLab Permissions

- ==Create, Update, Delete== Result
- ==Create, Update, Delete== Run
- ==Create, Update, Delete== Run Step
- ==Create, Update== Test Instance 
- ==Create, Update== Test Set 
- ==Create, Update== TestLab folder

[![testlab permissions](../img/soapui_project_setup/testlab-permissions-1.png)](../img/soapui_project_setup/testlab-permissions-1.png)