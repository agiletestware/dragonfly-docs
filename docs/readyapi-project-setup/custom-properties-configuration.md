In many ALM setups, users define custom user fields in various project entities such as Test, TestSets, Test Runs, etc. In such a setup, your ALM tests may have extra fields like in this screenshot

## ALM Custom User Fields
[![custom fields](../img/soapui_project_setup/2015-06-22_23h32_05.png)](../img/soapui_project_setup/2015-06-22_23h32_05.png)

Dragonfly makes it super easy to configure custom user fields by using a configuration wizard.
	
## Custom Fields Wizard

!!! warning "Error Message for non-configured projects"
    If an ALM project has `required` user properties and the ReadyAPI project has not mapped these user properties using the Dragonfly configuration wizard, you will see errors in the Dragonfly log tab during most of the operations.
	[![custom fields](../img/soapui_project_setup/2015-06-22_23h30_46.png)](../img/soapui_project_setup/2015-06-22_23h30_46.png)
	
To configure custom user fields, simple right click on your project and select ==Dragonfly: Configure Custom Fields==. This will open the custom fields configuration wizard.

[![custom fields wizard](../img/soapui_project_setup/2015-06-22_23h36_00.png)](../img/soapui_project_setup/2015-06-22_23h36_00.png)

The wizard allows you to configure ==Test==, ==TestSet== and ==Run== project entities. Simply specify the ALM Field Name, Label, and default value. Click ==Next== to configure other entities or click ==save== to save the mappings.

[![custom fields save](../img/soapui_project_setup/2015-06-22_23h38_13.png)](../img/soapui_project_setup/2015-06-22_23h38_13.png)


Once you click ==save==, the default values for each of the custom properties will be added to the ReadyAPI project in the following places

* ==Test:== ReadyAPI TestCase custom property 
* ==Testset:== ReadyAPI project and test suite custom property
* ==Run:== ReadyAPI  TestCase custom property

----

## Testcase Example:

[![testcase example](../img/soapui_project_setup/2015-06-23_00h14_56.png)](../img/soapui_project_setup/2015-06-23_00h14_56.png)

## Applying configurations to other projects

If you have more than one project defined in ReadyAPI and want to have the same custom properties configuration for several of your projects, you can easily achieve it by copying configuration from one ReadyAPI project to another.

To copy custom properties configuration:

 - Right click on your project and select ==Dragonfly: Configure Custom Fields==
 
 - Select ==Menu --> Copy from project==
 
[![copy configuration](../img/soapui_project_setup/custom_properties_copy_1.png)](../img/soapui_project_setup/custom_properties_copy_1.png)

 - Select a project whose configuration needs be copied and click ==OK== button
 
[![copy configuration](../img/soapui_project_setup/custom_properties_copy_2.png)](../img/soapui_project_setup/custom_properties_copy_2.png)

 - Click ==Save== button to apply changes

## Import/Export Configuration from/to file

Custom Properties Configuration can be exported to file and imported from it. This may be needed in case if you want to share your custom configuration with colleagues.

To export configuration:

 - Right click on your project and select ==Dragonfly: Configure Custom Fields==
 - Select ==Menu --> Export== 
 - Type in file name to which configuration should be stored and click ==Open==

To import configuration:

 - Right click on your project and select ==Dragonfly: Configure Custom Fields==
 - Select ==Menu --> Import==
 - Type in file name from which configuration should be read and click ==Open==
 - Click ==Save== button
