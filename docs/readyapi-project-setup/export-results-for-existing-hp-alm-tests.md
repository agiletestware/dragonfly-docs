In many companies, business analysts or manual QA engineers write tests in HP ALM. Some other team will then automate these tests using a variety of tools like ReadyAPI, WebDriver, etc. In such situations, it is very important to preserve these manual tests as well as allows engineers to use the tool of their choice.

Dragonfly allows you to map tests written in ReadyAPI to existing tests in HP ALM. As engineers run tests in ReadyAPI, Dragonfly will update the corresponding test case in HP ALM.

##Mapping between HP ALM tests and ReadyAPI tests

Mapping of a ReadyAPI test case to an existing test in HP ALM is a very simple process. Simply set the `QC_Mapped_Test_Id` custom property of a test case in ReadyAPI to the `Test ID` of a test case in HP ALM.

[![test mapping](../img/soapui_project_setup/ALM_Manual_test_mapping.png)](../img/soapui_project_setup/ALM_Manual_test_mapping.png)


[![test mapping existing](../img/soapui_project_setup/ReadyAPI_existing_test_mappings.png)](../img/soapui_project_setup/ReadyAPI_existing_test_mappings.png)


!!! note "Exporting ReadyAPI tests will not overwrite Tests in TestPlan"
    Dragonfly's default behavior is to overwrite the testcase in HP ALM's TestPlan when using ==project --> Dragonfly: Export to HP ALM== action menu. However, if the user decides to use this mapping feature, then the export feature will not overwrite anything. This is by design and is intended to preserve the HP ALM testcase in its original form.


##Viewing Mapped Test Results

Test results for mapped test cases is a bit different as compared to the general Dragonfly usage. In general Dragonfly usage, both test case and test step run results are synced to HP ALM. However, since it is not really possible to map a SOAP/REST/JDBC/etc test step in ReadyAPI to a design step in HP ALM, Dragonfly sets the run status of all the design steps in HP ALM based on the status of the test case. In addition, Dragonfly captures a lot of useful information about the ReadyAPI test case and dumps it into the test run's comments section

[![test mapping 1](../img/soapui_project_setup/ALM_Manual_mapping_1.png)](../img/soapui_project_setup/ALM_Manual_mapping_1.png)

[![test mapping 2](../img/soapui_project_setup/ALM_Manual_mapping_2.png)](../img/soapui_project_setup/ALM_Manual_mapping_2.png)

