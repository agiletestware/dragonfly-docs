Many organizations use HP ALM for tracking requirements. These organizations also like to use tools like ReadyAPI, WebDriver, Appium, JUnit, TestNG, etc for writing tests while still maintaining tractability of tests to requirements.

Dragonfly allows you to easily map ReadyAPI tests to the Requirements Module in HP ALM by simple testcase configurations.

To map ReadyAPI tests to HP ALM requirements, simply specify the ==ALM requirement ID== to ==QC_Req_Ids== custom property of a testcase in ReadyAPI.

[![requirements mapping](../img/soapui_project_setup/HP_ALM_requirements.png)](../img/soapui_project_setup/HP_ALM_requirements.png)

[![requirements](../img/soapui_project_setup/ReadyAPI_requirements_mapping.png)](../img/soapui_project_setup/ReadyAPI_requirements_mapping.png)

In some situations, a single test can cover more than one requirement. In such a situation, you can specify multiple Requirement IDs in ==QC_Req_Ids== as a ==comma-delimited string (e.g. 1,2,3)==.

In HP ALM, requirements may also have sub-requirements. This hierarchy can have many levels.
If there is a need to map a test case to all child requirements as well as to parent requirement, simply set ==QC_Req_Recursive== property to ==true== and dragonfly will map all child requirements to the testcase.

[![requirements map](../img/soapui_project_setup/ReadyAPI_requirements_mapping_1.png)](../img/soapui_project_setup/ReadyAPI_requirements_mapping_1.png)

Once you have set the requirement ID, export the ReadyAPI tests to HP ALM using Dragonfly's ==Export to HP ALM== action. Once exported, you should be able view the mapped requirements under the ==Req Coverage tab== of a test configuration in TestPlan:

[![requirements map](../img/soapui_project_setup/HP_ALM_requirement_mapping.png)](../img/soapui_project_setup/HP_ALM_requirement_mapping.png)

Once you run the test in ReadyAPI, corresponding requirements will automatically get updated. You will be able to view real-time requirements coverage report in HP ALM and find out how many of your requirements are passing. This is, by far, one of the best features of HP ALM !

[![requirements cov](../img/soapui_project_setup/HP_ALM_requirement_mapping_1.png)](../img/soapui_project_setup/HP_ALM_requirement_mapping_1.png)
