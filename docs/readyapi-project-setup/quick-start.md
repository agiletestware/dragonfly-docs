Follow these steps to get up and running in 10 minutes

---
## 1. Configure ReadyAPI/SoapUI project
Start ReadyAPI/SoapUI and lets configure your first project. To do so, right click on your project and select **"Dragonfly: (Un)Initialize project"**:

[![project configuration](../img/soapui_project_setup/initialize-project-1.png)](../img/soapui_project_setup/initialize-project-1.png)

and click on **"Yes"** button in confirmation screen:

[![project configuration](../img/soapui_project_setup/initialize-project-2.png)](../img/soapui_project_setup/initialize-project-2.png)

Now Dragonfly adds all required custom properties to your project, test suites and test cases.  
To remove them, just click on **"Dragonfly: (Un)Initialize project"** again and it will clear all Dragonfly related properties from your project.
 
The highlighted properties below are required and must be configured to reflect your ALM environment settings. 
[![project configuration](../img/soapui_project_setup/2015-06-06_14h23_24.png)](../img/soapui_project_setup/2015-06-06_14h23_24.png)

## 2. Export Project
Export ReadyAPI project into ALM. Simply right click the project and select ==Dragonfly: Export to HP ALM==. Dragonfly plugin will show a progress bar and live export status in the Dragonfly tab.

[![project export](../img/soapui_project_setup/2015-06-08_08h09_36.png)](../img/soapui_project_setup/2015-06-08_08h09_36.png)

[![faster export](../img/soapui_project_setup/2015-06-06_14h34_10.png)](../img/soapui_project_setup/2015-06-06_14h34_10.png)

!!! tip "Faster Exports"
    Export to ALM action can also be invoked from the ReadyAPI testsuite or testcase. Project action will export/update the entire project. Whereas, testsuite or testcase level actions will only export/update the specified testsuite and testcase respectively.

!!! note "Intelligent Export"
    users don't have to worry about manually creating any of the ==QC_TestPlanDirectory, QC_TestLabDirectory, or QC_TestSet==. Dragonfly is smart enough to figure this out and create it automatically at run time.

## 3. Validate Export
 The ReadyAPI tests should now be visible in the specified TestPlan directory. 

[![validate export](../img/soapui_project_setup/2015-06-06_14h55_27.png)](../img/soapui_project_setup/2015-06-06_14h55_27.png)


## 4a. Run tests from ReadyAPI

[![run tests from readyapi](../img/soapui_project_setup/2015-06-08_08h19_58.png)](../img/soapui_project_setup/2015-06-08_08h19_58.png)


## 4b. Run tests from HP ALM
To run the tests from HP ALM Testlab, create a testset and add tests cases to the testset just like you would when running HP UFT, QTP, etc. 

!!! warning "HP ALM Host setup requirements"
    When running ReadyAPI tests from HP ALM, the tests will run on some set of hosts selected by the user. The user must make sure that these hosts have all the necessary setup: ReadyAPI, dragonfly, licenses, and the ReadyAPI project.

!!! warning "TestLab path and Testset name in project custom properties"
    Make sure that the path you create in Testlab is the same path that is specified in your ReadyAPI project properties.
	

[![add to testset](../img/soapui_project_setup/2015-06-21_23h32_39.png)](../img/soapui_project_setup/2015-06-21_23h32_39.png)


[![run from ALM](../img/soapui_project_setup/2015-06-21_23h40_12.png)](../img/soapui_project_setup/2015-06-21_23h40_12.png)


## 5. Viewing Test Results in HP ALM TestLab

Go to the appropriate TestLab and TestSet. This page will show you the last ReadyAPI run summary.

[![view results](../img/soapui_project_setup/2015-06-22_06h21_04.png)](../img/soapui_project_setup/2015-06-22_06h21_04.png)


To view run details or to view run history, click on the testcase in the testset and select ==Runs==. The ==Runs== view shows all the runs for this test case. It is useful to see why a test case started failing. Click on the failing ==Run ID== to view the request and response for the various test steps.

[![view results](../img/soapui_project_setup/2015-06-22_06h32_48.png)](../img/soapui_project_setup/2015-06-22_06h32_48.png)

[![view results](../img/soapui_project_setup/2015-06-22_06h40_03.png)](../img/soapui_project_setup/2015-06-22_06h40_03.png)