ReadyAPI Layout Differences
===========================

---

Smartbear's ReadyAPI is the newest version of soapUI. ReadyAPI has a different UI and the Dragonfly menu will appear in different locations within ReadyAPI.

#### Project Level Menus

Menus such as **Export to ALM, Import from ALM, License Details**, etc will not appear under the** Project section** of ReadyAPI

![](../img/soapui_project_setup/readapi-project2.png)

#### TestCase and TestSuite Level Menus

Dragonfly plugin also exposes **Export to ALM** menu at the TestCase and TestSuite level. To view this menu in ReadyAPI, click on **soapUI NG section** and then click on the TestSuite or TestCase.

![](../img/soapui_project_setup/readapi-project3.png)

#### Dragonfly Log Tab

In ReadyAPI, **logs **are located at the bottom left hand corner. Click on **Logs **and then click on **Dragonfly Log** 

![](../img/soapui_project_setup/readapi-project4.png)

