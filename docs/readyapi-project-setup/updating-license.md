To update/add Dragonfly license, first exit out of ReadyAPI, and then place the new license file in ==%USERPROFILE%\dragonfly== folder.

[![update license](../img/soapui_project_setup/dragonfly_license_update.png)](../img/soapui_project_setup/dragonfly_license_update.png)