By default, Dragonfly updates test results in real-time. This means, Dragonfly will connect with HP ALM, upload test results, and then disconnect from ALM for every testcase.

Projects with large number of tests or teams with slow connection to HP ALM, may encounter increased project execution time due to real-time updates. 

If this overhead is not desireable, Dragonfly's ==Bulk Update== feature allows users to ==update all tests at the end of the project==. Using this approach, Dragonfly plugin makes a single connection with HP ALM, parallelly updates tests, and then a disconnects from HP ALM.


!!! success "HP ALM Performance Analysis"
    In HP ALM, the most expensive operation is authentication. Authentication alone can take 1000-2000 ms. Bulk updates addresses this problem by authenticating only once.

To use bulk update feature, follow these two steps.

## 1. Update QC_BulkUpdate
Change ==QC_BulkUpdate== project property to ==enabled==

[![project properties](../img/soapui_project_setup/2015-06-08_23h59_26.png)](../img/soapui_project_setup/2015-06-08_23h59_26.png)


## 2. Run ReadyAPI tests
Run ReadyAPI tests as you would. You will see message in the dragonfly log tab indicating that bulkupdate is enabled and test results are being written to file.

[![dragonfly log](../img/soapui_project_setup/2015-06-09_00h01_23.png)](../img/soapui_project_setup/2015-06-09_00h01_23.png)

Select `Dragonfly: Bulk Results Update` project menu. Dragonfly will now process all the test results and upload them to HP ALM.

[![bulkupdate menu](../img/soapui_project_setup/2015-06-08_23h59_26.png)](../img/soapui_project_setup/2015-06-09_00h03_36.png)

---

## Storing Multiple Runs
By default, Dragonfly only stores the latest run when bulk update is enabled, but sometimes it is necessary to store all runs and then export them to HP ALM.

To do that, set ==QC_Bulk_Update== to ==enabled_all_runs==. Once enabled, Dragonfly will save all runs locally in some directory. Once done with testing, simply export all runs to HP ALM by using the ==Dragonfly: Bulk Results Update== from the project menu

[![bulkupdate all runs](../img/soapui_project_setup/Bulk_update_all_runs.png)](../img/soapui_project_setup/Bulk_update_all_runs.png)

[![bulkupdate](../img/soapui_project_setup/Bulk_update_all_runs_1.png)](../img/soapui_project_setup/Bulk_update_all_runs_1.png)

---


If you want to clear latest run results, select ==Dragonfly: Information== from the project menu and then select ==Clear bulk results==

[![bulkupdate all](../img/soapui_project_setup/Bulk_update_all_runs_2.png)](../img/soapui_project_setup/Bulk_update_all_runs_2.png)

