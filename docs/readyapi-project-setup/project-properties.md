List of project properties and their functions

---
!!! tip "TestSuite properties"
    Some properties like ==QC_TestPlanDirectory==, ==QC_TestLabDirectory==, ==QC_TestSet== can also be configured at the testsuite level. If specified at the testsuite level, the testsuite value will take precedence over project values.
	
!!! note
    Specifying these values at the test suite is very useful when users want to map each test suite to a different testplan, testlab, testset.
	


| Property                 | Purpose                            | Sample value                     | Required |
| ------------------------ | ---------------------------------- | -------------------------------- | -------- |
| QC_URL                   | HP ALM URL. Must end with ==/qcbin== | http://52.8.167.151:8080/qcbin | Yes      |
| QC_Domain                | HP ALM domain name                 | default                        | Yes      |
| QC_Project               | HP ALM project name                | demo-project                   | Yes      |
| QC_user                  | HP ALM user name. The user must <br> have the following permissions in ALM <br> <ul><li>add tests</li><li>add delete design steps</li><li>add test plan</li><li>add testset</li><li>add/delete runs</li></ul> | ==jsmith== | Yes |
| QC_password              | HP ALM password value. The <br> password is encrypted and masked <br> automatically. | | Yes |
| QC_TestPlanDirectory     | HP ALM Test Plan path. If the path <br> does not exist, Dragonfly will create it <br> automatically. | `Subject\testplan01` | Yes |
| QC_TestLabDirectory      | HP ALM Test Lab path. ReadyAPI test <br> results will be saved in this location. If <br> the path does not exist, Dragonfly will <br> create it automatically. | `Root\testlab01` | Yes |
| QC_TestSet               | HP ALM Testset value. ReadyAPI test <br> results will be saved in this location. If <br> the testset does not exist, Dragonfly <br> will create it automatically. | `myTestSet01` | Yes |
| QC_soapUI_ARGS           | When running ReadyAPI via command <br> line via continuous integration tools <br> like Jenkins or Bamboo or when <br> running ReadyAPI from HP ALM, users <br> can specify any testrunner.bat <br> arguments in this parameter. `note: most users will not need this setup` | `"-j" "-fC:\Reports"` | No |
| QC_BulkUpdate            | If you think that Dragonfly adds too <br> much overhead and you need to <br> speed up your test execution, you can <br> speed up the process by doing a bulk <br> update at the end of the test run. <br> Please read [Bulk Update configuration](bulk-update.md) for detailed instructions. | `enabled` <br> `disabled` | No |
| QC_RunName               | Overrides default run name. <br><br> Default format is ==Run: <time stamp> <br> syncing complete== | `release-1.6-run` | No |
| QC_ProjectPath           | If running ReadyAPI tests via HP ALM, <br> carefully specify this value. By default, <br> when tests are exported to HP ALM, <br> Dragonfly takes the **user's local ReadyAPI** <br> **project location** and dynamically <br> creates VBS script in HP ALM test case. <br><br> In ALM, tests can run on a variety of <br> hosts. The user should make sure that <br> all the hosts have the ReadyAPI project in <br> the specified location. <br><br> We recommend that user's use git, <br> svn, etc to synchronize the code on <br> ALM hosts. You can also store ReadyAPI <br> project in some network path. | `//10.12.32.23/ReadyAPI/` <br><br> `C:\ReadyAPI-projects\\` | No |
| QC_soapUIPath            | When running ReadyAPI tests via HP <br> testlab, dragonfly uses ReadyAPI's <br> ==testrunner.bat== and default to the <br> user's ReadyAPI location. <br><br> If the ALM nodes don't have ReadyAPI <br> installed in the same location as the <br> exporting user, you should specify the <br> path to testrunner.bat | `D:\ReadyAPI\bin\testrunner.bat` | No |
| QC_Export_Disabled_Tests | If enabled, all tests (disabled + <br> enabled) will be exported to ALM testset. | `enabled` <br><br> `disabled` | No |
| QC_Dragonfly_Plugin      | Enables or disables the Dragonfly <br> plugin. Useful when you don't want <br> tests to be automatically exported <br> while running. | `enabled` <br><br> `disabled` | No |
| QC_Override_Test_Runs    | If enabled, removes all previous test runs in HP ALM so test has only one run. | `enabled` <br><br> `disabled` | No |


