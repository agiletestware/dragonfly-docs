In some organizations, it is very common teams to do the initial test design in HP ALM TestPlan. Once the application is ready, the team writes automated tests using a variety of testing tools like Selenium, UFT, ReadyAPI, etc.

Dragonfly plugin allows you to import tests from ALM Test Plan into a ReadyAPI project and creates a skeleton project - test cases with no test steps.

The user then creates the test logic by adding various ReadyAPI test steps. Once the test authoring is complete, simply export the project to HP ALM using the ==Dragonfly=Export to HP ALM== menu. Using this approach, teams can convert plan in HP ALM and automated those plans in ReadyAPI

!!! warning "Export to HP ALM action will ==DELETE ALL== design steps in HP ALM"
    ==Dragonfly: Export to HP ALM== project menu always deletes the existing design steps in ALM test case and re-creates ReadyAPI's testcase steps. This is done to ensure that the ReadyAPI tests are in sync with HP ALM testcase.
	
To import a test cases from HP ALM, simply configure the ==QC_TestPlanDirectory== property in your ReadyAPI project and select ==Dragonfly: Import from HP ALM==. This will create testsuites and testcases in the ReadyAPI project.

[![dragonfly menu](../img/soapui_project_setup/2015-06-23_00h29_38.png)](../img/soapui_project_setup/2015-06-23_00h29_38.png)

[![import log](../img/soapui_project_setup/2015-06-23_00h52_03.png)](../img/soapui_project_setup/2015-06-23_00h52_03.png)


!!! tip "ALM Folder Hierarchy"
    Dragonfly Import action creates ReadyAPI test suites based on folders in ALM TestPlan. ==QC_TestPlanDirectory== represents the root folder during the import process. The import process expects only one level of child folders and all the tests to be in these child folders. The child folders map to test suites in ReadyAPI. If tests in the ALM Test Plan are inside the root folder or if the child folders have other child folders, the import process cannot create the appropriate structure in ReadyAPI. Instead, all the tests from ==QC_TestPlanDirectory== are imported into a generic test suite ==ALM_Imported_TestCases==


[![import suite](../img/soapui_project_setup/2015-06-23_01h17_27.png)](../img/soapui_project_setup/2015-06-23_01h17_27.png)


[![import dump](../img/soapui_project_setup/2015-06-23_01h17_40.png)](../img/soapui_project_setup/2015-06-23_01h17_40.png)

