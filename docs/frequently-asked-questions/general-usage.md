**How can I turn off or disable dragonfly temporarily?**
<br>
To disable Dragonfly, simply change ==QC_Dragonfly_Plugin== to ==disabled==

**How can I report an issue and what kind of information do I need to provide?**
<br>
If you encounter any issues with Dragonfly, send your issue to via [contact-us page ](https://www.agiletestware.com/contact-us "contact-us"). Include the issue description, and attached the ==dragonfly.log== located in ==%userprofile%/dragonfly==

**How can I get my license information or update the license?**
<br>
Right click on on any project and select ==Dragonfly:License==. The license can be located and updated in the ==Dragonfly Location==

[![license info](../img/faq/2015-06-23_02h15_08.png)](../img/faq/2015-06-23_02h15_08.png)

**I am evaluating Dragonfly, what kind of support do I get during evaluation?**
<br>
You get the same support as enterprise customers at no charge. We want to make sure that you love the product before you purchase.
