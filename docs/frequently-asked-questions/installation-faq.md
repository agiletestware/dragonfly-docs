**Installation failed due to missing ==soapui== or ==soapuios== folders**

[![Installation](../img/faq/2015-07-01_21h34_40.png)](../img/faq/2015-07-01_21h34_40.png)

soapUI Pro 5.12 and higher, ReadyAPI, and soapUI Open Source 5.2 and higher all create ==.soapui== or ==soapuios== folders inside ==user.home==. Dragonfly installer will copy the plugin files inside this folder as required by the soapUI plugin architecture. If this folder is missing, it means that you may be on a deprecated version of soapUI. You can either upgrade to latest version of soapUI or use Dragonfly version 4.5
