## License Model
What is the license model for Agiletestware Dragonfly?

Dragonfly has two licensing models:

==Basic License:== This can be used by upto 5 users/machines and comes with basic email support.

==Enterprise License:== This can be used by any number of people/machines within the organiztion. There are no restrictions. Enterprise license includes premium support with 48 hour response time SLA (email + phone + gotomeeting support)

## Quote and Invoices
How can I get formal quote or invoice for Agiletestware?

Please send your request to [Agiletestware Sales ](https://www.agiletestware.com/contact-us "Contact Sales"). Once we get your inquiry, our sales team will send over a quote or invoice.

## Purchase Orders
Can I purchase Dragonfly using a Purchase Order?

Yes. Please send your PO to  [Agiletestware Sales ](https://www.agiletestware.com/contact-us "Contact Sales")

## Orders
I just placed an order and did not get my license key. How do I get the license key ?

It usually takes 12-30 hours for the keys to be generated and sent via email. Please contact  [Agiletestware Sales](https://www.agiletestware.com/contact-us "Contact Sales") if you have not received your keys.

## Resellers
Does Agiletestware work with resellers?

We work with various resellers. We currently work with [SHI International](https://www.shi.com "SHI International"), [Softchoice](https://www.softchoice.com/ "Softchoice"), [Aquion](http://www.aquion.com.au/ "Aquion"), [Software One](https://www.softwareone.com "Software One"), [mclicense](http://www.mclicense.de/en/ "mclicense"), [e-business Design](http://www.ebusiness-design.com/ "e-business Design").