Dragonfly works on all versions of soapUI, ReadyAPI, and HP ALM. 

If you are planning on upgrading HP ALM to a newer version, please follow these steps for Dragonfly upgrade.

1. Backup your current dragonfly license. The license is located in ==%userprofile%/dragonfly==
2. Uninstall Dragonfly by running dragonfly-uninstall.exe in ==%userprofile%/dragonfly==
3. Install HP Quality Center Connectivity Add-in from ALM management tools. Typically ==http://<alm url>:8080/qcbin/addins.html==
4. Re-Install Dragonfly using the latest installer.
