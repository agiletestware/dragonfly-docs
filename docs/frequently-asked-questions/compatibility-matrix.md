Dragonfly works with pretty much all versions of HP ALM and soapUI and readyAPI

|                               | HP QC 10.x                | HP ALM 11.x               | HP ALM 12.x               | HP ALM SAAS               | Download Link |
| ----------------------------- | ------------------------- | ------------------------- | ------------------------- | ------------------------- | ------------- |
| Ready API 1.4.x               | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | [Download Dragonfly](https://www.agiletestware.com/dragonfly#download) |
| soapUI Pro NG 5.12 +          | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | [Download Dragonfly](https://www.agiletestware.com/dragonfly#download) |
| soapUI Open Source 5.2 +      | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | [Download Dragonfly](https://www.agiletestware.com/dragonfly#download) |
| soapUI 4.5, 5.0, 5.1 (legacy) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | ![](../img/faq/green.png) | [Download Dragonfly](https://www.agiletestware.com/dragonfly#download) |
 
