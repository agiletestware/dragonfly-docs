Release Notes
=============
**Dragonfly ver: 6.1.7**

Bugs fixed:
	
	[DRAG-199] - Installer requires elevation while it should not
	[DRAG-200] - JMS step: description is not updated in HP ALM
	[DRAG-202] - If plugin is disabled, it still generates groovy log files which causes issues in load tests
	[DRAG-208] - Plugin always fails running test suite regardless of its status

**Dragonfly ver: 6.1.6**

New Features:
	
	[DRAG-187] - Export XML Response for JDBC steps
	[DRAG-191] - DataSource loop - fail step if any of iteration failed and add actual value for each iteration
	[DRAG-193] - Export results on project/suite run (not for each test case)
	[DRAG-196] - Add property for disabling attachments upload

Bugs fixed:
	
	[DRAG-190] - Store real execution date for Bulk Update
	[DRAG-197] - Fix compatibility with ReadyAPI 2.5.0


**Dragonfly ver: 6.1.5**

New Features:

    [DRAG-188] - Remove events tracking.

**Dragonfly ver: 6.1.4**

New Features:

    [DRAG-180] - Replace automatic injection of QC_ fields on project load by "Dragonfly: (Un)Initialize project" menu item to workaround some ReadyAPI 2.3.0 bug with environments.
    [DRAG-181] - Add support for JMS test step    

Bugs:

    [DRAG-179] - "java.io.IOException: Could not create file" is throws under some circumstances   

**Dragonfly ver: 6.1.3**

Bugs:

	[DRAG-175] - NullPointerException during export of test result in ReadyAPI 2.2.0
	[DRAG-176] - NoClassDefFound error in sending tracking data after bulk update	
	
**Dragonfly ver: 6.1.2**

Bugs:

	[DRAG-171] - Dragonfly log tab is not visible in ReadyAPI 2.2.0
	
**Dragonfly ver: 6.1.1**

Bugs:

	[DRAG-166] - Installer: If installer runs twice, then uninstaller does not delete dragonfly jars
	[DRAG-168] - NullPointerException during export of the project to testplan when bulkupdate is enabled

**Dragonfly ver: 6.1.0**

Bugs:

    [DRAG-161] - NullPointerException occurrs during export of results for SOAP steps, under some circumstances

**Dragonfly ver: 6.0.9**

New Features:

    [DRAG-122] - Signed installers
    [DRAG-153] - Do not focus on "Dragonfly" log tab when exporting test results

Bugs:

    [DRAG-157] - Test is added to an arbitrary requirement if QC_Req_Recursive is set to true
    [DRAG-159] - HTTP response, containing PDF file, may break exporting results to HP ALM
    
**Dragonfly ver: 6.0.8**

New Features:

    [DRAG-148] - Mapping ReadyAPI test to HP ALM Requirements
    [DRAG-148] - Mapping of ReadyAPI tests to existing tests in HP ALM
    [DRAG-146] - Automatic handling of Fast Runs in HP ALM

Bugs:

    [DRAG-144] - Check testplan when updating testcase with non-empty qc_id
    [DRAG-150] - hp alm version control can create duplicate runs
    [DRAG-149] - java.lang.illegalStateExcpetion after long running session

**Dragonfly ver: 6.0.7**

New Features:

	[DRAG-16] - provide option 'override_test_runs' in project level

Bugs:

	[DRAG-103] - Modifying a saved custom property creates a new test case property

**Dragonfly ver: 6.0.6**

New Features:

    [DRAG-16] - provide option 'override_test_runs' in project level
    [DRAG-66] - export return values from teststep of type: testcase
    [DRAG-77] - automatically focus on dragonfly log tab
    [DRAG-82] - Support for reading properties from environments
    [DRAG-89] - capture full REST request query in testlab
    [DRAG-93] - Custom Property Wizard clone
    [DRAG-95] - Update testcase name only if QC_ID is found
    [DRAG-102] - ALM Custom Property Configurationwizard
    [DRAG-106] - Report back properties steps values in ALM
    [DRAG-116] - When using paths in project properties, check for trailing unicode value when combining properties.
    [DRAG-124] - Attach groovy log to the test step in ALM
    [DRAG-125] - Check Discard OK results and Max Results properties in test case settings and raise warning
    [DRAG-126] - Change request/response attachment to .txt and use .xml where it is really XML
    [DRAG-130] - Update run duration in HP ALM
    [DRAG-138] - Change custom properties dialog
    [DRAG-127] - Detect type of request/response and append appropriate file extensions
    [DRAG-128] - import/export project mapping configuration


Bugs:

    [DRAG-103] - Modifying a saved custom property creates a new test case property
    [DRAG-129] - Populating of dragonfly properties does not work when environments are used
    [DRAG-132] - When user had blank password it over written by the dummy one on project load
    [DRAG-133] - Run from HP ALM: when dragonfly failed to launch the status is Passed
    [DRAG-134] - Dragonfly does not work if user profile name has spaces
    [DRAG-135] - Value of QC_soapUI_ARGS is not exported to VB script
    [DRAG-136] - Error messages is not displayed when REST/SOAP/HTTP call failed with error
    [DRAG-137] - Custom Properties: Value for existing properties are cleared when user defines empty default value and save configuration
    [DRAG-140] - NPE during exporting test results if project does not have mapped custom fields

**Dragonfly ver: 6.0.5**

New Features:

    [DRAG-85] - New license manager
    [DRAG-88] - Dragonfly support for attachments in request steps
    [DRAG-90] - Improve auto save feature
    [DRAG-92] - Store results of several runs and bulk export them into ALM
    [DRAG-97] - Easy navigation to open dragonfly logs
    [DRAG-100] - Code Sign Jars from Smartbear
    [DRAG-104] - Clear bulk results
    [DRAG-109] - Dragonfly should delete only fast runs and do not throw exception if deletion fails
    [DRAG-112] - Capture Headers in request and response attachments
    [DRAG-119] - Add exporting of groovy script failure message to ALM
    [DRAG-90] - Improve auto save feature

Bugs:

    [DRAG-79] - Improved error handling when having more than one license files
    [DRAG-34] - Dragonfly: Problems with encoding in response XML.
    [DRAG-79] - Improved error handling when having more than one license files
    [DRAG-68] - Non-ascii symbols are stored incorrectly in ALM
    [DRAG-114] - Change assertion status from VALID to PASSED
    [DRAG-113] - Capture test run expected, actual and description