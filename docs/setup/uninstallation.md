To uninstall Dragonfly from your PC, please follow the next steps:

*	Shutdown ReadyAPI\SoapUI
*	Run ==%USERPROFILE%\dragonfly\dragonfly-uninstall.exe== . Usually, it is ==C:\Users\user\dragonfly\dragonfly-uninstall.exe==
*	Click ==Next== button
* 	Wait for uninstaller to remove all components from the system
*	Click on ==Finish== button

![](../img/setup/uninstall-1.png)  
  
  
![](../img/setup/uninstall-2.png) 